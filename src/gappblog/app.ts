'use strict';

module gappblog {
  export var app = angular.module("gappblog", [
    'ngRoute',
    'ngSanitize',
    'restangular'
  ]);
  /**
   * Configuration settings for the app
   */
  app.config([
    '$routeProvider',
    'RestangularProvider',
    ($routeProvider, RestangularProvider) => {
      // Configure the routes for each of our panes
      $routeProvider
        .when('/', {
            templateUrl: 'views/entries.html',
            controller: 'EntryCtrl'
        })
        .otherwise({
            redirectTo: '/'
        });
      RestangularProvider.setBaseUrl(
        'http://192.168.1.92:8000/blog'
      ).setRequestSuffix('/');
    }
  ]);
}
