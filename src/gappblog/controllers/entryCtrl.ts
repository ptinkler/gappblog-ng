'use strict';

module gappblog.controllers {
  interface IEntryCtrlScope extends ng.IScope {
    vm: EntryCtrl;
  }

  class EntryCtrl {
    static $inject: string[] = [
      '$scope',
      'Restangular'
    ];
    entries: any;
    constructor(
      private $scope: IEntryCtrlScope,
      private Restangular: restangular.IService
    ){
      $scope.vm = this;

      Restangular.all('entries').getList().then(
        (entries: any) => {
          this.entries = entries;
        }
      )
    }
  }

  app.controller("EntryCtrl", EntryCtrl);
}