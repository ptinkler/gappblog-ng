'use strict';

describe('Controller: Main', function () {

  // load the controller's module
  beforeEach(module('gappblog'));

  var Main, scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope: ng.IScope) {
    scope = $rootScope.$new();
    Main = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(5);
  });
});
